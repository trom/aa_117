## {{ standards-size-heading[Standards placement heading] <span>03</span> Size }}

{{ standards-size There are strict sizes for individual ads, as well as the total space occupied by ads. All ads that are visible in the browser window when the page first loads must not collectively occupy more than 15% of the visible portion of the web page. If placed lower on the page, ads must not collectively occupy more than 25% of the visible portion of the webpage. }}

{{ standards-size-list Ads must also comply with size limitations, according to the ad position: }}

  - {{ standards-size-200px-high 200px high when above the Primary Content }}
  - {{ standards-size-350px-wide 350px wide when on the side of the Primary Content }}
  - {{ standards-size-400px-high 400px high when placed below the Primary Content }}
