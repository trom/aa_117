### {{ text-ads-heading[heading] <span>01</span> Text ads}}

{{ text-ads Text ads designed with excessive use of colors and/or other elements to grab attention are not permitted. }}

### {{ image-ads-heading[heading] <span>02</span> Image ads }}

{{ image-ads-static Static image ads may qualify as acceptable, according to an evaluation of their unobtrusiveness based on their integration on the webpage. }}

### {{ in-feed-ads-heading[heading] <span>03</span> In-feed ads }}

{{ in-feed-ads For ads in lists and feeds, the general criteria differ depending on: }}

#### {{ in-feed-ads-placement-requirements-heading[heading] PLACEMENT REQUIREMENTS }}

{{ in-feed-ads-placement-requirements Ads are permitted in between entries and feeds. }}

#### {{ in-feed-ads-size-requirements-heading[heading] SIZE REQUIREMENTS }}

{{ in-feed-ads-size-requirements In-feed ads are permitted to take up more space, as long as they are not substantially larger than other elements in the list or feed. }}

### {{ search-ads-heading[heading] <span>04</span> Search ads }}

{{ search-ads For search ads—ads displayed following a user-initiated search query—the criteria differ depending on: }}

#### {{ search-ads-size-requirements-heading[heading] SIZE REQUIREMENTS }}

{{ search-ads-size-requirements Search ads are permitted to be larger and take up additional screen space. }}

### {{ no-primary-content-heading[heading] <span>05</span> Ads on pages with no Primary Content }}

{{ no-primary-content Only text ads are allowed. For webpages without any Primary Content (e.g. error or parking pages), the criteria differ depending on: }}

#### {{ no-primary-content-placement-requirements-heading[heading] PLACEMENT REQUIREMENTS }}

{{ no-primary-content-placement-requirements No placement limitations. }}

#### {{ no-primary-content-size-requirements-heading[heading] SIZE REQUIREMENTS }}

{{ no-primary-content-size-requirements No size limitations. }}
