# {{ what-are-aa-heading[heading] What are Acceptable Ads? }}

{{ what-are-aa-1 Acceptable Ads are ads that abide by criteria specially developed for and by ad-blocking users. They are respectful, nonintrusive, and relevant. Over 150 million ad-blocking users have consented to seeing Acceptable Ads. }}

[{{ read-more[button text] Read more }}](#without){: .button .green }
